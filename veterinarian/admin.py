from django.contrib import admin

from .models import VeterinarianModel

# Register your models here.
admin.site.register(VeterinarianModel)
